# WP Timber / ACF / WPML BOILERPLATE

## Timber
page.php minimal theme  
single.php contains examples for ACF etc.
 
## ACF
ACFs are defined in acf-definitions.php  
A global Options page is predefined

## Menus 
3 Menu locations defined: Main, Social and Meta
