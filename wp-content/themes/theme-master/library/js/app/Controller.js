/**
 * Author: GNOMJOGSON
 * Date: 23.06.2015
 * Created: 17:09
 **/
 (function(window){

    Controller.prototype.constructor = Controller;
    Controller.prototype = {
        deviceType: "", //computer || tablet || phone
        pageId: "", //wordpress page ID
        themePath: "", //path to wordpress theme
        breakpoints:{ // screen resolutions breakpoints
            screen_xs: 480,
            screen_sm: 768,
            screen_md: 1030,
            screen_lg: 1240,
            screen_xlg: 1440
        }
    }

    var $, ref, swiperArray, swiperMobileIsInitialized, swiperSettings ;
    function Controller(jQuery){

        $ = jQuery;
        ref= this;

        Logger.useDefaults();
        //Logger.setLevel(Logger.OFF);

        swiperArray = [];
        swiperMobileIsInitialized = false;

    }

    Controller.prototype.init = function(){

        this.deviceType = window.deviceType;
        this.pageId = window.currentPageId;
        this.themePath = window.themePath;

        Logger.info("Startup site on deviceType: " + this.deviceType + " pageId: " + this.pageId + ", width: " + ref.viewport().width + ", height: " + ref.viewport().height + ", screensize: " + ref.viewport().screensize);

        //resize handler
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();

        $(window).resize(function() {
            delay(function(){
                ref.resize();
            }, 50);
        });

        // Init der burgermenu 
        ref.initMenu();

        /// Responsive Video-Embeds
        $('.embed-responsive').fitVids();


        // Royalslider for images
        $("#image-gallery").royalSlider({
            // general options go gere
            keyboardNavEnabled: true,
            visibleNearby: {
                enabled: true,
                centerArea: 0.5,
                center: true,
                breakpoint: 650,
                breakpointCenterArea: 0.64,
                navigateByCenterClick: true
            }
        });

        // Alternative Swiper/Slider 
        ref.initSwiper();

        ref.resize();   

    };

    Controller.prototype.initSwiper = function() {

        $(".swiper-container").each(function(index, element){

            var $this = $(this);
            $this.addClass("instance-" + index);
            $this.find(".swiper-button-prev").addClass("btn-prev-" + index);
            $this.find(".swiper-button-next").addClass("btn-next-" + index);
            $this.find(".swiper-scrollbar").addClass("swiper-scrollbar-" + index);
            
            var swiperSettings = {
                touchEventsTarget: ".instance-" + index,
                scrollbar: ".swiper-scrollbar-" + index,
                scrollbarHide: false,
                scrollbarDraggable: true,
                scrollbarSnapOnRelease: true,
                nextButton: ".btn-next-" + index,
                prevButton: ".btn-prev-" + index,
                grabCursor: true,
                loop: false,
                centeredSlides: true,
                slidesPerView: 1,
            };

            if($this.hasClass('swiper-carousel')) {                
                swiperSettings.loop = true;
                swiperSettings.slidesPerView = 'auto';
            }

            var swiperObject = new Swiper(".instance-" + index, swiperSettings);
            
            var swiper = [];
            swiper.push(swiperObject);
            swiper.push(swiperSettings);
            swiperArray.push(swiper);

            // hide & show prev next buttons on hover 
            var btns = ".btn-prev-" + index + ", .btn-next-" + index;
            $(btns).addClass('swiper-button-hidden');

            $(".instance-" + index).on('mouseleave',function (){
                $(btns).removeClass('swiper-button-visible');
                $(btns).addClass('swiper-button-hidden');
            });

            $(".instance-" + index + " .swiper-slide").on('mousemove',function (e){
                var parentOffset = $(this).parent().offset();
                var relX = e.pageX - parentOffset.left;

                if( relX < ($(this).width() / 2) ) {
                    if(!$(".btn-prev-" + index).hasClass('swiper-button-visible')) {
                        $(".btn-next-" + index).removeClass('swiper-button-visible').addClass('swiper-button-hidden');
                        $(".btn-prev-" + index).addClass('swiper-button-visible').removeClass('swiper-button-hidden');
                    }
                } else {
                    if(!$(".btn-next-" + index).hasClass('swiper-button-visible')) {
                        $(".btn-prev-" + index).removeClass('swiper-button-visible').addClass('swiper-button-hidden');
                        $(".btn-next-" + index).addClass('swiper-button-visible').removeClass('swiper-button-hidden');
                    }

                }
            });

            ref.swiperMobileIsInitialized = true;

        });

    };

    Controller.prototype.initMenu = function() {

        var toggles = document.querySelectorAll(".mobile-menu-toggle");
        
        for (var i = toggles.length - 1; i >= 0; i--) {
          var toggle = toggles[i];
          toggleHandler(toggle);
      };

      function toggleHandler(toggle) {
          toggle.addEventListener( "click", function(e) {
            e.preventDefault();
            (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
            $('body').toggleClass('mobile-menu-open');
        });
      }
  };

  Controller.prototype.resize = function()
  {
        // resized to/in mobile
        if((ref.viewport().screensize == 'screen_xs') || (ref.viewport().screensize == 'screen_xxs'))
        {

            // coming from desktop (swiper not yet initialized)
            // if(!ref.swiperMobileIsInitialized) {
            //     // create all mobile swipers
            //     for (var i = swiperArray.length - 1; i >= 0; i--) {
            //         if(swiperArray[i][0].container.hasClass('swiper-mobile-only')){
            //             swiperArray[i][0] = new Swiper(swiperArray[i][0].container,swiperArray[i][1]);
            //         }
            //     }
            //     ref.swiperMobileIsInitialized = true;
            // }

            // gradient overlays on agency page
            //ref.initContentReveal();

        // resize to/in desktop
        } else {

            // reset the menu
            $('body').removeClass('mobile-menu-open');
            $(".mobile-menu-toggle").removeClass('is-active');

            // // coming from mobile
            // if(ref.swiperMobileIsInitialized) {

            //     // destroy all mobile only swipers
            //     for (var i = swiperArray.length - 1; i >= 0; i--){
            //       if(swiperArray[i][0].container.hasClass('swiper-mobile-only')){
            //         swiperArray[i][0].destroy(true, true);
            //     }
            // }

            // ref.swiperMobileIsInitialized = false;
        }
    };

    /*
     *
     * GENERIC HELPERS - GETTER/SETTER FUNCTIONS
     *
     * */

    //this returns the "real" windows width/height as used in media queries (returns Object{ width:x, height:y })
    Controller.prototype.viewport = function()
    {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }

        var screensize = "screen_xxs";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_xs) screensize = "screen_xs";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_sm) screensize = "screen_sm";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_md) screensize = "screen_md";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_lg) screensize = "screen_lg";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_xlg) screensize = "screen_xlg";

        return { width : e[ a+'Width' ] , height : e[ a+'Height' ], screensize : screensize };
    };

    window.Controller = Controller;

}(window));
