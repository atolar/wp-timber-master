<?php
/*
 Template Name: Front Page
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

			<div id="content">
				<div id="inner-content">
                        <div class="wrap">
                            <div class="row">
                                <div id="start" class="section col-xs-24">
                                    <div class="vertically-center">
                                        <h1 class="headline"><?php the_title(); ?></h1>
                                        <h3 class="subline"><?php echo get_field('subline'); ?></h3>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="wrap">
                        <div class="row">
                            <div id="about" class="section col-xs-12">
                                <p><?php echo get_field('about'); ?></p>
                            </div>
                        </div>
                        </div>
                        <div class="wrap-fullwidth">
                            <!-- images -->
                            <div class="row">
                                <div id="media" class="section col-xs-24">

                                    <div class="row">
                                        <div id="image-gallery "class="royalSlider rsDefault visibleNearby rsHor">
                                            <?php if( have_rows('images') ): ?>
                                                <?php while( have_rows('images') ): the_row();
                                                    // vars
                                                    $image = get_sub_field('images_image');
                                                    $desc = get_sub_field('images_description');
                                                    ?>
                                                    <a class="rsImg" href="<?php echo $image['url']; ?>" ><?php echo $desc; ?></a>
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap">
                                <!-- videos -->
                                <div class="row">
                                    <div class="col-xs-24">
                                        <?php if( have_rows('videos') ): ?>

                                            <ul>
                                                <?php while( have_rows('videos') ): the_row();
                                                    // vars
                                                    $url = get_sub_field('videos_url');
                                                    $embed_code = wp_oembed_get( $url, '' );
                                                    $desc = get_sub_field('videos_description');
                                                    ?>

                                                    <li class="col-xs-8">
                                                        <div class="embed-responsive"><?php /*echo $embed_code;*/ ?></div>
                                                        <p><?php echo $desc; ?></p>
                                                    </li>

                                                <?php endwhile; ?>

                                            </ul>

                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <div class="wrap">
                            <div class="row">
                                <div id="education" class="section col-xs-12">
                                    <p><?php echo get_field('education'); ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div id="contact" class="section col-xs-24">
                                    <div class="vertically-center">
                                        <p><?php echo get_field('contact'); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
				</div>
			</div>


<?php get_footer(); ?>
