<?php

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

// Querying custom post types
//
// $args = array( 	
// 	'post_type' => array('services') , 
// 	'orderby' => 'menu_order',
// 	'order' => 'ASC',
// 	'nopaging' => true
// 	);
// $context['services'] = Timber::get_posts( $args );



// fetching post object of a page defined on ACF Global Options page
//
// $contact_page_ID = get_field('global_contact_page','option')[0];
// $context['contact'] = new TimberPost($contact_page_ID);


// fetiching full post objects from ACF relation field 
// 
// foreach (get_field('home_references',$post->ID) as $key => $home_reference) {
//	$context['teasers'][$key] = new TimberPost($home_reference->ID);
// }

// changing title and content to somerhing else 
//
// $context['teasers'][$key]->title = get_field('agency_headline_people',$agency_page_ID);
// $context['teasers'][$key]->content = get_field('agency_text_people',$agency_page_ID);

Timber::render( 'views/page-services.twig', $context );

?>

